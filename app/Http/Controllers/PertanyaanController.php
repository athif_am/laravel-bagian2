<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create() {
        return view('items.form');
    }

    public function store(Request $request ) {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
            ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan!');
    }

    public function index() {
        $gets = DB::table('pertanyaan')->get();
        return view('items.pertanyaan',compact('gets'));
    }

    public function show($pertanyaan_id) {
        $get = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('items.show',compact('get'));
    }

    public function edit($pertanyaan_id) {
        $get = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('items.edit',compact('get'));
    }

    public function update($pertanyaan_id, Request $request) {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);
        $query = DB::table('pertanyaan')
                    ->where('id', $pertanyaan_id)
                    ->update([
                        "judul" => $request["judul"],
                        "isi" => $request["isi"]
                        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil diupdate!');
    }

    public function destroy($pertanyaan_id) {
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus!');
    }
}