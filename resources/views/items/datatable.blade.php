@extends('layouts.master')

@section('content')

@include ('contents.datatable')

@endsection

@push('scripts')

<script src="{{ asset('/lte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('/lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>

@endpush