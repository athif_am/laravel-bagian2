<div class="ml-3 ml-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan {{$get->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{$get->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul Pertanyaan</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul', $get->judul)}}" placeholder="Enter Title">
                    @error('judul')
                    <div class="alert alert-danger">{{message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi">Pertanyaan Yang Diajukan</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{old('isi', $get->isi)}}" placeholder="Isi Pertanyaan">
                    @error('isi')
                    <div class="alert alert-danger">{{message}}</div>
                    @enderror
                  
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
</div>