<div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                <div class="alert alert-success">
                  {{ session('success') }}
                </div>
                @endif
                <a class="btn btn-primary mb-2" href="/pertanyaan/create"> Buat Pertanyaan Baru</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Pertanyaan</th>
                      <th style="width: 40px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                      @forelse ($gets as $key => $get)
                      <tr>
                          <td> {{ $key + 1 }} </td>
                          <td> {{ $get->judul }} </td>
                          <td> {{ $get->isi}} </td>
                          <td style="display: flex;"> 
                          <a href="/pertanyaan/{{$get->id}}" class="btn btn-info btn-sm">detail</a>
                          <a href="/pertanyaan/{{$get->id}}/edit" class="btn btn-default btn-sm">edit</a>
                          <form action="/pertanyaan/{{$get->id}}" method="post">
                          @csrf 
                          @method('DELETE')
                          <input type="submit" value="delete"class="btn btn-danger btn-sm">
                          </form>
                          </td>
                      </tr>
                      @empty
                      <tr>
                          <td colspan="4" align="center"> Tidak Ada Pertanyaan</td>
                      </tr>
                      @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>